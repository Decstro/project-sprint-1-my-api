/* eslint linebreak-style: ["error", "windows"] */
const express = require('express');

const router = express();
require('../db');

router.use(express.json());

const Usuario = require('../models/usuarios.model');

const verificarIfAdmin = async (req, res, next) => {
  const { username } = req.user;
  try {
    const usuario = await Usuario.findOne({ username, ifadmin: true });
    if (!usuario) res.status(401).json(`El usuario ${username} no es administrador`);
    else next();
  } catch (err) {
    res.status(401).json(`El usuario ${username} no es administrador`);
  }
};

module.exports = verificarIfAdmin;
