/* eslint linebreak-style: ["error", "windows"] */
const express = require('express');

const router = express();
const _ = require('lodash');
const clienteRedis = require('../redis');

router.use(express.json());

const cache = async (req, res, next) => {
  clienteRedis.get('Productos', (error, respuesta) => {
    if (error) res.json(error);
    if (respuesta) res.json(_.groupBy(JSON.parse(respuesta), 'tipo'));
    else next();
  });
};

module.exports = cache;
