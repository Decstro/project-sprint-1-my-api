/* eslint linebreak-style: ["error", "windows"] */
const express = require('express');

const router = express();
require('../db');

router.use(express.json());

const Usuario = require('../models/usuarios.model');

const probarIfActive = async (req, res, next) => {
  const { username } = req.user;
  try {
    const usuario = await Usuario.findOne({ username });
    if (usuario.ifActive) next();
    else res.status(401).json(`El usuario ${username} se encuentra suspendido`);
  } catch (err) {
    res.status(401).json(`El usuario ${username} se encuentra suspendido`);
  }
};

module.exports = probarIfActive;
