/* eslint linebreak-style: ["error", "windows"] */
const swaggerOptions = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Sprint Project 1 - Protalento',
      description: 'API de Acamica para DWBE',
      version: '1.0.0',
    },
    servers: [
      {
        url: '../',
        description: 'Local Server',
      },
    ],
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
        },
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  },
  apis: ['./src/routes/*.js', './components.yaml'],

};

module.exports = swaggerOptions;
