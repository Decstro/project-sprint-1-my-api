/* eslint-disable no-underscore-dangle */
/* eslint-disable no-console */
/* eslint linebreak-style: ["error", "windows"] */
const express = require('express');

const router = express();
require('../db');

const Producto = require('../models/productos.model');
const Pedido = require('../models/pedidos.model');

router.use(express.json());

async function agregarProductoVerVacio(req, res, next) {
  const { nombre, precio, tipo } = req.body;

  if (nombre === '' || nombre === null) {
    res.status(400).json('El campo nombre se encuentra vacio');
  } else if (precio === '' || precio === null) {
    res.status(400).json('El campo precio se encuentra vacio');
  } else if (tipo === '' || tipo === null) {
    res.status(400).json('El campo tipo se encuentra vacio');
  } else if (tipo === 'Bebidas' || tipo === 'FastFood' || tipo === 'Asados') {
    try {
      const producto = await Producto.findOne({ nombre });
      if (!producto) {
        if (precio > 0) {
          next();
        } else res.status(400).json(`El precio no puede ser negativo o cero ${precio}`);
      } else res.status(400).json('Error el nombre del producto ya se encuentra registrado');
    } catch (err) {
      console.error(err);
    }
  } else res.status(400).json('Solo existen estos tres tipos de productos: Bebidas, FastFood, Asados');
}

async function editarProductoVerBody(req, res, next) {
  const { nombre, id, precio } = req.body;

  if (nombre === '' || nombre === null) {
    res.status(400).json('El campo nombre se encuentra vacio');
  } else if (precio === '' || precio === null) {
    res.status(400).json('El campo precio se encuentra vacio');
  } else if (id === '' || id === null) {
    res.status(400).json('El campo id se encuentra vacio');
  } else {
    try {
      const producto = await Producto.findOne({ nombre });
      if (producto === null) {
        if (precio >= 0) {
          try {
            const actualizarProducto = await Producto.findById(id);
            if (actualizarProducto) next();
            else res.status(400).json(`No existe un producto con este id: ${id}`);
          } catch (err) {
            res.status(400).json(`No existe un producto con este id: ${id}`);
          }
        } else res.status(400).json('El precio no puede ser negativo');
      } else res.status(400).json(`El producto: ${nombre} ya existe`);
    } catch (err) {
      res.status(400).json(`No existe un producto con este nombre ${nombre}`);
    }
  }
}

async function verExistenciaProducto(req, res, next) {
  // eslint-disable-next-line prefer-destructuring
  const carro = req.body.carro;
  let contador = 0;
  try {
    const productos = await Producto.find();

    carro.forEach((product) => {
      productos.forEach((producto) => {
        if (product.nombre === producto.nombre) contador += 1;
      });
    });

    if (contador !== carro.length) res.status(400).json('Al menos 1 producto de los ingresados no existe en el restaurante');
    else next();
  } catch (err) {
    res.json('No se encontraron productos');
  }
}

async function productController(req, res, next) {
  const { idPedido, nombreProducto, cantidad } = req.body;

  if (idPedido === '' || idPedido === null) {
    res.status(400).json('El campo idPedido se encuentra vacio');
  } else if (nombreProducto === '' || nombreProducto === null) {
    res.status(400).json('El campo nombreProducto se encuentra vacio');
  } else if (cantidad === '' || cantidad === null) {
    res.status(400).json('El campo cantidad se encuentra vacio');
  } else if (cantidad < 0) {
    res.status(400).json('El campo cantidad  no puede ser menor a cero');
  } else {
    try {
      const pedido = await Pedido.findOne({ _id: idPedido });
      try {
        const productos = await Producto.findOne({ nombre: nombreProducto });
        if (productos) {
          const producto = pedido.carrito.find((u) => u.nombre === nombreProducto);
          if (producto) {
            try {
              const product = await pedido.carrito.id(producto._id);
              if (product) next();
            } catch (err) {
              res.json(`Error al buscar el id:  ${producto._id} `);
            }
          } else next();
        } else res.json(`El producto ${nombreProducto} no existe  en la base de datos de productos `);
      } catch (err) {
        console.error(err);
      }
    } catch (err) {
      res.json(`No existe un pedido con este id ${idPedido}`);
    }
  }
}

module.exports = {
  agregarProductoVerVacio,
  editarProductoVerBody,
  verExistenciaProducto,
  productController,
};
