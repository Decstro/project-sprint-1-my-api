/* eslint-disable no-underscore-dangle */
/* eslint-disable no-console */
/* eslint linebreak-style: ["error", "windows"] */
const express = require('express');

const router = express();
require('../db');

const Usuario = require('../models/usuarios.model');
const Pedido = require('../models/pedidos.model');

router.use(express.json());

async function agregarPedidoVerVacio(req, res, next) {
  const {
    username,
    idDireccion,
    carro,
    nombreMedioPago,
  } = req.body;

  if (username === '' || username === null) {
    res.status(400).json('El campo username se encuentra vacio');
  } else if (carro.length === 0) {
    res.status(400).json('El campo carro se encuentra vacio');
  } else if (nombreMedioPago === '' || nombreMedioPago === null) {
    res.status(400).json('El campo nombreMedioPago se encuentra vacio');
  } else if (idDireccion === '' || idDireccion === null) {
    res.status(400).json('El campo idDireccion se encuentra vacio');
  } else {
    try {
      const usuario = await Usuario.findOne({ username });
      if (usuario) {
        try {
          const direccionUsuario = await usuario.direcciones.id(idDireccion);
          if (!direccionUsuario) {
            res.status(400).json(`No existe una dirección con este id: ${idDireccion} `);
          } else next();
        } catch (err) {
          res.status(400).json(`El usuario ${username} no tiene una direccion con este id: ${idDireccion} `);
        }
      } else res.json(`No existe el usuario ${username}`);
    } catch (err) {
      res.status(400).json(`El usuario:  ${username} no existe en la base de datos`);
    }
  }
}

async function borrarProductoVerBody(req, res, next) {
  const { idPedido, nombreProducto } = req.body;
  if (idPedido === '' || idPedido === null) res.status(400).json('El campo idPedido se encuentra vacio');
  else if (nombreProducto === '' || nombreProducto === null) res.status(400).json('El campo nombreProducto se encuentra vacio');
  else {
    try {
      const pedido = await Pedido.findOne({ _id: idPedido });
      if (pedido.estado === 'Pendiente') {
        try {
          const producto = pedido.carrito.find((u) => u.nombre === nombreProducto);
          const products = await pedido.carrito.id(producto._id);
          if (products) {
            next();
          } else res.status(400).json(`No se pudo eliminar el producto porque el usuario  en el pedido: ${idPedido} no tiene un producto con este nombre: ${nombreProducto}`);
        } catch (err) {
          res.status(400).json(`No existe en el pedido: ${idPedido} este producto: ${nombreProducto}`);
        }
      } else res.status(400).json(`El pedido ${idPedido}, no se puede editar porque su estado es: ${pedido.estado}`);
    } catch (err) {
      res.status(400).json(`No existe un pedido con este id: ${idPedido}`);
    }
  }
}

async function editarCantidadVerBody(req, res, next) {
  const { idPedido, nombreProducto, cantidad } = req.body;
  if (idPedido === '' || idPedido === null) res.status(400).json('El campo idPedido se encuentra vacio');
  else if (nombreProducto === '' || nombreProducto === null) res.status(400).json('El campo nombreProducto se encuentra vacio');
  else if (cantidad === '' || cantidad === null) res.status(400).json('El campo cantidad se encuentra vacio');
  else {
    try {
      const pedido = await Pedido.findOne({ _id: idPedido });
      if (pedido.estado === 'Pendiente') {
        const producto = pedido.carrito.find((u) => u.nombre === nombreProducto);
        if (producto) {
          const products = await pedido.carrito.id(producto._id);
          if (products) {
            next();
          } else res.status(404).json(`No se pudo editar la cantidad del producto porque el pedido: ${idPedido} no tiene un producto con el nombre: ${nombreProducto}`);
        } else res.status(404).json(`El producto ${nombreProducto} no existe en el pedido  ${idPedido}`);
      } else res.status(404).json(` El pedido no se puede editar porque su estado es: ${pedido.estado}`);
    } catch (err) {
      res.status(404).json(` El usuario  no tiene un pedido con este id: ${idPedido}`);
    }
  }
}

async function agregarProductoVerBody(req, res, next) {
  const { idPedido, nombreProducto, cantidad } = req.body;

  if (idPedido === '' || idPedido === null) res.status(400).json('El campo idPedido se encuentra vacio');
  else if (nombreProducto === '' || nombreProducto === null) res.status(400).json('El campo nombreProducto se encuentra vacio');
  else if (cantidad === '' || cantidad === null) res.status(400).json('El campo cantidad se encuentra vacio');
  else {
    try {
      const pedido = await Pedido.findOne({ _id: idPedido });
      if (pedido.estado === 'Pendiente') {
        next();
      } else res.status(404).json(` El pedido no se puede editar porque su estado es: ${pedido.estado}`);
    } catch (err) {
      res.status(404).json(` El usuario  no tiene un pedido con este id: ${idPedido}`);
    }
  }
}

async function cerrarPedidoVerBody(req, res, next) {
  const { idPedido } = req.body;
  if (idPedido === '' || idPedido === null) res.status(400).json('El campo idPedido se encuentra vacio');
  else {
    try {
      const pedido = await Pedido.findOne({ _id: idPedido });
      if (pedido.estado === 'Pendiente') {
        next();
      } else res.status(404).json(` El pedido no se puede cerrar porque ya esta cerrado su estado es: ${pedido.estado}`);
    } catch (err) {
      res.status(404).json(` No existe un pedido con este id: ${idPedido}`);
    }
  }
}

async function obtenerPedidosVer(req, res, next) {
  try {
    const pedido = await Pedido.find();
    if (pedido.length === 0) {
      res.json('Actualmente no hay pedidos en el sistema');
    } else next();
  } catch {
    res.json('Error, no se pueden obtener los pedidos');
  }
}

async function editarEstadoVerBody(req, res, next) {
  const { idPedido, estado } = req.body;
  if (idPedido === '' || idPedido === null) res.status(400).json('El campo idPedido se encuentra vacio');
  else if (estado === '' || estado === null) res.status(400).json('El campo estado se encuentra vacio');
  else if (estado !== 'Pendiente' && estado !== 'Confirmado' && estado !== 'En preparación' && estado !== 'Enviado' && estado !== 'Entregado') res.status(400).json(`Error el estado (${estado}) del pedido no es valido`);
  else {
    try {
      const pedido = await Pedido.findOne({ _id: idPedido });
      if (pedido) next();
      else res.json(`El usuario no tiene un pedido con este id: ${idPedido}`);
    } catch {
      res.json(`El usuario no tiene un pedido con este id: ${idPedido}`);
    }
  }
}

module.exports = {
  agregarPedidoVerVacio,
  borrarProductoVerBody,
  editarCantidadVerBody,
  agregarProductoVerBody,
  cerrarPedidoVerBody,
  obtenerPedidosVer,
  editarEstadoVerBody,
};
