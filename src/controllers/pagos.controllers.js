/* eslint-disable no-console */
/* eslint linebreak-style: ["error", "windows"] */
const express = require('express');

const router = express();
require('../db');

const Pago = require('../models/mediosDePago.model');
const Usuario = require('../models/usuarios.model');

router.use(express.json());

async function agregarPagoVerVacio(req, res, next) {
  const { nombre, tipo } = req.body;
  if (nombre === '' || nombre === null) {
    res.status(400).json('El campo nombre se encuentra vacio');
  } else if (tipo === '' || tipo === null) {
    res.status(400).json('El campo tipo se encuentra vacio');
  } else if (tipo !== 'PSE' && tipo !== 'Cuenta de Ahorros' && tipo !== 'Efectivo' && tipo !== 'Cheque' && tipo !== 'Cuenta Corriente') {
    res.status(400).json('Error en el tipo de medio de pago, solo existen estos tipos: PSE, Cuenta de Ahorros, Efectivo, Cheque y Cuenta Corriente');
  } else {
    try {
      const pago = await Pago.findOne({ nombre });
      if (!pago) next();
      else res.status(400).json(`El nombre: ${nombre} ya se encuentra registrado en los Medios de Pago`);
    } catch (err) {
      console.error(err);
    }
  }
}

async function editarPagoVerBody(req, res, next) {
  const { nombre, id, tipo } = req.body;
  if (nombre === '' || nombre === null) {
    res.status(400).json('El campo nombre se encuentra vacio');
  } else if (id === '' || id === null) {
    res.status(400).json('El campo id se encuentra vacio');
  } else if (tipo === '' || tipo === null) {
    res.status(400).json('El campo tipo se encuentra vacio');
  } else if (tipo !== 'PSE' && tipo !== 'Cuenta de Ahorros' && tipo !== 'Efectivo' && tipo !== 'Cheque' && tipo !== 'Cuenta Corriente') {
    res.status(400).json('Error en el tipo de medio de pago, solo existen estos tipos: PSE, Cuenta de Ahorros, Efectivo, Cheque y Cuenta Corriente');
  } else {
    try {
      const pago = await Pago.findOne({ nombre });
      if (!pago) {
        try {
          const agregarMedioPago = await Pago.findById(id);
          if (agregarMedioPago) next();
        } catch (err) {
          res.json(`No existe un producto con este id: ${id} `);
        }
      } else res.status(400).json(`El nombre: ${nombre} ya se encuentra registrado en los Medios de Pago`);
    } catch (err) {
      console.error(err);
    }
  }
}

async function eliminarPagoVerBody(req, res, next) {
  const id = req.params.idMedioPago;
  if (id === '' || id === null) res.status(400).json('El campo id se encuentra vacio');
  else {
    try {
      const eliminarPago = await Pago.findById(id);
      if (eliminarPago) next();
      else res.status(400).json(`No existe un medio de pago con este id: ${id}`);
    } catch (err) {
      res.status(400).json(`No existe un medio de pago con este id: ${id}`);
    }
  }
}

async function verPagoUser(req, res, next) {
  const { username, nombreMedioPago } = req.body;
  try {
    const user = await Usuario.findOne({ username });
    if (user) {
      try {
        const mediosPago = await Pago.findOne({ nombre: nombreMedioPago });
        if (mediosPago) next();
        else res.status(400).json(`Este medio de Pago: ${nombreMedioPago} no existe en la base de datos`);
      } catch (err) {
        res.status(400).json(`Este medio de Pago: ${nombreMedioPago} no existe en la base de datos`);
      }
    } else res.status(400).json(`Este usuario: ${username} no existe en la base de datos`);
  } catch (err) {
    res.status(400).json(`Este usuario: ${username} no existe en la base de datos`);
  }
}

module.exports = {
  agregarPagoVerVacio,
  editarPagoVerBody,
  eliminarPagoVerBody,
  verPagoUser,
};
