/* eslint-disable no-underscore-dangle */
/* eslint-disable no-console */
/* eslint linebreak-style: ["error", "windows"] */
const express = require('express');

const router = express();
const bcrypt = require('bcrypt');
require('../db');

const Usuario = require('../models/usuarios.model');

router.use(express.json());

async function verUserVacio(req, res, next) {
  const {
    username,
    password,
    email,
    age,
    telefono,
    direcciones,
  } = req.body;

  if (username === '' || username === null) {
    res.status(400).json('El campo username se encuentra vacio');
  } else if (password === '' || password === null) {
    res.status(400).json('El campo password se encuentra vacio');
  } else if (email === '' || email === null) {
    res.status(400).json('El campo email se encuentra vacio');
  } else if (direcciones.length === 0) {
    res.status(400).json('El usuario no tiene direcciones');
  } else if (age === '' || age === null) {
    res.status(400).json('El campo age se encuentra vacio');
  } else if (age <= 0) {
    res.status(400).json('Edad equivocada');
  } else if (telefono === '' || telefono === null) {
    res.status(400).json('El campo telefono se encuentra vacio');
  } else if (telefono < 1000000000) {
    res.status(400).json('El telefono debe tener 10 digitos y no puede ser negativo');
  } else {
    try {
      const usuarioEmail = await Usuario.findOne({ email });
      if (usuarioEmail) res.status(400).json(`El email: ${email} ya se encuentra registrado`);
      else {
        try {
          const usuario = await Usuario.findOne({ username });
          if (usuario) res.status(400).json(`El usuario: ${username} ya se encuentra registrado`);
          else next();
        } catch (err) {
          console.error(err);
        }
      }
    } catch (err) {
      console.error(err);
    }
  }
}

async function verSuspension(req, res, next) {
  const { username, password } = req.params;
  try {
    const {
      username: userUsername,
      password: userPassword,
      ifActive,
    } = await Usuario.findOne({ username });

    if (userUsername) {
      const resultado = bcrypt.compareSync(password, userPassword);
      if (resultado) {
        if (ifActive) next();
        else res.status(401).json(`El usuario  ${username} se encuentra suspendido`);
      } else res.status(400).json('Contraseña incorrecta');
    } else res.status(400).json('Usuario inexistente en la base de datos');
  } catch (err) {
    console.error(err);
    res.status(400).json('Usuario  incorrecto');
  }
}

async function suspension(req, res, next) {
  const { username } = req.body;
  try {
    const user = await Usuario.findOne({ username });
    if (user.ifActive) next();
    else res.status(401).json(`El usuario  ${username} se encuentra suspendido`);
  } catch (err) {
    res.status(401).json(`Usuario inexistente ${username}`);
  }
}

module.exports = {
  verUserVacio,
  verSuspension,
  suspension,
};
