/* eslint linebreak-style: ["error", "windows"] */
const redis = require('redis');
const config = require('./config');

// eslint-disable-next-line prefer-destructuring
const clienteRedis = redis.createClient(config.module.RedisPORT);

module.exports = clienteRedis;
