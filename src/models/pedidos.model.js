/* eslint-disable no-console */
/* eslint linebreak-style: ["error", "windows"] */
const mongoose = require('mongoose');

const pedidoSchema = new mongoose.Schema({
  IdUser: {
    type: String,
    required: true,
  },
  estado: {
    type: String,
    default: 'Pendiente',
  },
  direccion: {
    type: String,
    required: true,
  },
  precio: {
    type: Number,
    default: 0,
  },
  nombreMedioPago: {
    type: String,
    required: true,
  },
  carrito: [{
    nombre: {
      type: String,
      required: true,
    },
    cantidad: {
      type: Number,
      default: 1,
    },
    precio: {
      type: Number,
      default: 0,
    },
  }],
});

module.exports = mongoose.model('Pedido', pedidoSchema);
