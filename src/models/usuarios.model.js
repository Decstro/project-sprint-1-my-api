/* eslint-disable no-console */
/* eslint linebreak-style: ["error", "windows"] */
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const config = require('../config');

const usuarioSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  age: {
    type: Number,
    required: true,
  },
  telefono: {
    type: Number,
    required: true,
  },
  direcciones: [{
    direccion: {
      type: String,
      required: true,
    },
  }],
  ifActive: {
    type: Boolean,
    default: true,
  },
  ifadmin: {
    type: Boolean,
    default: false,
  },
});

const Usuario = mongoose.model('Usuario', usuarioSchema);

(async () => {
  const decision = await Usuario.findOne({ ifadmin: true });
  if (!decision) {
    const admin = new Usuario(
      {
        username: config.module.adminUser,
        password: bcrypt.hashSync(config.module.adminPassword, 5),
        email: 'decstro@gmail.com',
        age: 21,
        telefono: 3148234672,
        direcciones: [{ direccion: 'Torices, Carrera 14 #41-33' }, { direccion: 'Manga, Avenida de las Mercedes #72-30' }],
        ifadmin: true,
      },
    );

    admin.save();
    console.log(`MongoDB: Usuario administrador creado - Primer ingreso ${admin}`);
  }
})();

module.exports = mongoose.model('Usuario', usuarioSchema);
