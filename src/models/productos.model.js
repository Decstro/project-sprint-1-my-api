/* eslint-disable no-console */
/* eslint linebreak-style: ["error", "windows"] */
const mongoose = require('mongoose');

const productoSchema = new mongoose.Schema({
  nombre: {
    type: String,
    required: true,
  },
  precio: {
    type: Number,
    required: true,
  },
  tipo: {
    type: String,
    required: true,
  },
});

const Producto = mongoose.model('Producto', productoSchema);

(async () => {
  const decision = await Producto.find();
  if (decision.length === 0) {
    const producto1 = new Producto({ nombre: 'Hamburguesa Doble', precio: 12000, tipo: 'FastFood' });
    producto1.save();

    const producto2 = new Producto({ nombre: 'ChoriPerro', precio: 9000, tipo: 'FastFood' });
    producto2.save();

    const producto3 = new Producto({ nombre: 'Hamburguesa Sencilla', precio: 9000, tipo: 'FastFood' });
    producto3.save();

    const producto4 = new Producto({ nombre: 'Lomo de Cerdo', precio: 12000, tipo: 'Asados' });
    producto4.save();

    const producto5 = new Producto({ nombre: 'Pechuga', precio: 10000, tipo: 'Asados' });
    producto5.save();

    const producto6 = new Producto({ nombre: 'Carne', precio: 15000, tipo: 'Asados' });
    producto6.save();

    const producto7 = new Producto({ nombre: 'Pescado', precio: 14000, tipo: 'Asados' });
    producto7.save();

    const producto8 = new Producto({ nombre: 'Limonada', precio: 4000, tipo: 'Bebidas' });
    producto8.save();

    const producto9 = new Producto({ nombre: 'Corona', precio: 5000, tipo: 'Bebidas' });
    producto9.save();

    const producto0 = new Producto({ nombre: 'CocaCola', precio: 4000, tipo: 'Bebidas' });
    producto0.save();

    console.log('MongoDB: Productos creados por defecto - Primer ingreso');
  }
})();

module.exports = mongoose.model('Producto', productoSchema);
