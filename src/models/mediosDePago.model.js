/* eslint-disable no-console */
/* eslint linebreak-style: ["error", "windows"] */
const mongoose = require('mongoose');

const medioPagoSchema = new mongoose.Schema({
  nombre: {
    type: String,
    required: true,
  },
  tipo: {
    type: String,
    required: true,
  },
});

const Pago = mongoose.model('Pago', medioPagoSchema);

(async () => {
  const decision = await Pago.find();
  if (decision.length === 0) {
    const medioPago1 = new Pago({ nombre: 'Tarjeta Visa', tipo: 'PSE' });
    medioPago1.save();

    const medioPago2 = new Pago({ nombre: 'Ahorros Bancolombia', tipo: 'Cuenta de Ahorros' });
    medioPago2.save();

    const medioPago3 = new Pago({ nombre: 'Tarjeta MasterCard', tipo: 'Cuenta Corriente' });
    medioPago3.save();

    const medioPago4 = new Pago({ nombre: 'Billetes', tipo: 'Efectivo' });
    medioPago4.save();

    const medioPago5 = new Pago({ nombre: 'Cheque Bancario', tipo: 'Cheque' });
    medioPago5.save();

    console.log('MongoDB: Medios de Pago creados por defecto - Primer ingreso');
  }
})();

module.exports = mongoose.model('Pago', medioPagoSchema);
