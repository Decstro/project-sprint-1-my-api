/* eslint linebreak-style: ["error", "windows"] */
const mongoose = require('mongoose');
const config = require('./config');

// eslint-disable-next-line prefer-destructuring
(async () => {
  // eslint-disable-next-line no-unused-vars
  const db = await mongoose.connect(config.module.DBConnection, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  // eslint-disable-next-line no-console
  console.log('Conectado a la DB');
})();
