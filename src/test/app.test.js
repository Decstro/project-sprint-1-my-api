/* eslint-disable no-undef */
/* eslint linebreak-style: ["error", "windows"] */
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index');
const Usuario = require('../models/usuarios.model');

chai.should();
chai.use(chaiHttp);

describe('Registro de Usuarios', () => {
  describe('POST /register', () => {
    it('Debe devolver un 200 en status', (done) => {
      const usuario = {
        username: 'BjornIronSide',
        password: 'ImTheKing',
        email: 'forKattegat@gmail.com',
        age: 55,
        telefono: 2356782532,
        direcciones: [
          {
            direccion: 'Kattegat Carrera 94 #40-29',
          },
          {
            direccion: 'Norway avenue Manzana 94 Lote 5, #40-29',
          },
        ],
      };

      chai.request(app)
        .post('/login-register')
        .send(usuario)
        .end((err, response) => {
          response.should.have.status(200);
          response.should.be.an('object');
          done();
        });
      after(async () => {
        await Usuario.deleteOne({ email: 'forKattegat@gmail.com' });
      });
    });
  });
});
