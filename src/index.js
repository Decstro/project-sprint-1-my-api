/* eslint linebreak-style: ["error", "windows"] */
const express = require('express');
const helmet = require('helmet');
require('./db');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const expressJwt = require('express-jwt');
const config = require('./config');
const middlewareSuspension = require('./middlewares/ifActive.middlewares');
const swaggerOptions = require('./utils/swaggerOptions');
const usuarioRoutes = require('./routes/usuario.route');
const pedidoRoutes = require('./routes/pedido.route');
const productoRoutes = require('./routes/producto.route');
const mediosPagosRoutes = require('./routes/mediosPago.route');
const ingresarRoutes = require('./routes/loginRegister.route');

const environment = process.env.NODE_ENV;
const apiDescription = process.env.API_DESCRIPTION;

const app = express();
app.use(express.json());

// Swagger
const swaggerSpecs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));
app.use('/login-register', ingresarRoutes);

app.use(helmet());

app.use(
  expressJwt({
    secret: config.module.mySecretPassword,
    algorithms: ['HS256'],
  }).unless({
    path: ['/login-register'],
  }),
);

// eslint-disable-next-line no-unused-vars
app.use((err, _req, res, _next) => {
  if (err.name === 'UnauthorizedError') res.status(401).json('Token Invalido');
  else res.status(500).json('Internal Server Error');
});

app.use(middlewareSuspension);

app.use('/usuarios', usuarioRoutes);
app.use('/pedidos', pedidoRoutes);
app.use('/productos', productoRoutes);
app.use('/mediosPago', mediosPagosRoutes);

// eslint-disable-next-line no-console
app.listen(config.module.PORT, () => {
  console.log(`Escuchando desde el puerto:  http://localhost:${config.module.PORT}`);
  console.log(`The application is running in the '${environment}' enviroment`);
  console.log(`Description: '${apiDescription}'`);
});

module.exports = app;
