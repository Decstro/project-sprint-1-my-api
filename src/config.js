/* eslint linebreak-style: ["error", "windows"] */
/* eslint-disable import/newline-after-import */
const { config } = require('dotenv');
config();

exports.module = {
  PORT: process.env.PORT,
  RedisPORT: process.env.REDISPORT,
  DBConnection: process.env.DBCONNECTION,
  mySecretPassword: process.env.SECRETPASSWORD,
  adminUser: process.env.ADMINUSER,
  adminPassword: process.env.ADMINPASSWORD,
//   DBURL: process.env.DBURL,
//   DB_port: process.env.DB_PORT,
//   DB_user: process.env.DB_USER,
//   DB_password: process.env.DB_PASSWORD,
//   DB_database: process.env.DB_DATABASE,
//   REDISURL: process.env.REDISURL,
};
