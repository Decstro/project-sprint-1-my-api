/* eslint linebreak-style: ["error", "windows"] */
const express = require('express');

const router = express();
const _ = require('lodash');
const autenticarAdministrador = require('../middlewares/probarAdmin.middlewares');
const { agregarPagoVerVacio, editarPagoVerBody, eliminarPagoVerBody } = require('../controllers/pagos.controllers');
const Pago = require('../models/mediosDePago.model');

/**
 * @swagger
 * /mediosPago/obtenerMediosPago:
 *  get:
 *      summary: Obtener todos los medios de pago del sistema
 *      tags: [MediosPago]
 *      responses:
 *          200:
 *              description: Lista de medios de pago del sistema organizados por tipo
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/MedioPago'
 */
router.get('/obtenerMediosPago', autenticarAdministrador, async (req, res) => {
  const mediosPago = await Pago.find();
  res.json(_.groupBy(mediosPago, 'tipo'));
});

/**
 * @swagger
 * /mediosPago/crearMedioPago:
 *   post:
 *       summary: Crea un nuevo medio de pago en el sistema
 *       tags: [MediosPago]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/agregarMedioPago'
 *       responses:
 *           200:
 *               description: Medio de Pago creado exitosamente
 *           400:
 *               description: Error al digitar los datos
 *
 */
router.post('/crearMedioPago', autenticarAdministrador, agregarPagoVerVacio, async (req, res) => {
  const { nombre, tipo } = req.body;
  const agregarMedioPago = new Pago({ nombre, tipo });
  agregarMedioPago.save();
  res.json('Medio de Pago creado exitosamente');
});

/**
 * @swagger
 * /mediosPago/editarMedioPago:
 *   put:
 *       summary: Edita el nombre y el precio de un producto
 *       tags: [MediosPago]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/editarMedioPago'
 *       responses:
 *           200:
 *               description: Medio de pago editado exitosamente
 *           400:
 *               description: Error al digitar los datos
 *
 */
router.put('/editarMedioPago', autenticarAdministrador, editarPagoVerBody, async (req, res) => {
  const { nombre, id, tipo } = req.body;
  const agregarMedioPago = await Pago.findById(id);
  agregarMedioPago.nombre = nombre;
  agregarMedioPago.tipo = tipo;
  agregarMedioPago.save();
  res.json('Medio de Pago actualizado exitosamente');
});

/**
 * @swagger
 * /mediosPago/eliminarMedioPago/{idMedioPago}:
 *   delete:
 *       summary: Elimina el medio de pago segun el id enviado por parametros
 *       tags: [MediosPago]
 *       parameters:
 *         - name: idMedioPago
 *           in: path
 *           required: true
 *           description: Id del medio de pago a eliminar
 *           schema:
 *               type: string
 *       responses:
 *           200:
 *               description: Elimina un medio de pago del sistema
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/editarMedioPago'
 *           400:
 *               description: Id del medio de pago inexistente
 *
 */
router.delete('/eliminarMedioPago/:idMedioPago', autenticarAdministrador, eliminarPagoVerBody, async (req, res) => {
  const id = req.params.idMedioPago;
  const eliminarPago = await Pago.findByIdAndDelete(id);
  // eslint-disable-next-line no-console
  console.log(eliminarPago);
  res.json('Medio de Pago eliminado exitosamente');
});

/**
 * @swagger
 * tags:
 *  name: MediosPago
 *  description: Sección de medios de pago del sistema
 * components:
 *  schemas:
 *      MedioPago:
 *          type: object
 *          required:
 *              -nombre
 *              -tipo
 *          properties:
 *              nombre:
 *                  type: string
 *                  description: Nombre del medio de pago
 *              tipo:
 *                  type: string
 *                  description: Categoria del medio de pago
 *          example:
 *              nombre: Tarjeta Debito Visa
 *              tipo: PSE
 *      agregarMedioPago:
 *          type: object
 *          required:
 *              -nombre
 *              -tipo
 *          properties:
 *              nombre:
 *                  type: string
 *                  description: Nombre del medio de pago
 *              tipo:
 *                  type: string
 *                  description: Categoria del medio de pago
 *          example:
 *              nombre: Black Card
 *              tipo: PSE
 *      editarMedioPago:
 *          type: object
 *          required:
 *              -id
 *              -nombre
 *              -tipo
 *          properties:
 *              id:
 *                  type: string
 *                  description: Id del pago a editar
 *              nombre:
 *                  type: string
 *                  description: Nombre del medio de pago
 *              tipo:
 *                  type: string
 *                  description: Categoria del medio de pago
 *          example:
 *              id: 611f27c1b2dfd01a64bb9ef4
 *              nombre: BlueCard
 *              tipo: PSE
 *
 */
module.exports = router;
