/* eslint-disable eqeqeq */
/* eslint-disable no-underscore-dangle */
/* eslint-disable prefer-destructuring */
/* eslint linebreak-style: ["error", "windows"] */
const express = require('express');

const router = express();
const _ = require('lodash');
require('../db');
const autenticarAdministrador = require('../middlewares/probarAdmin.middlewares');
const {
  agregarPedidoVerVacio,
  borrarProductoVerBody,
  editarCantidadVerBody,
  agregarProductoVerBody,
  cerrarPedidoVerBody,
  obtenerPedidosVer,
  editarEstadoVerBody,
} = require('../controllers/pedidos.controllers');

const {
  verExistenciaProducto,
  productController,
} = require('../controllers/productos.controllers');

const { verPagoUser } = require('../controllers/pagos.controllers');
const { suspension } = require('../controllers/users.controllers');

const Usuario = require('../models/usuarios.model');
const Pedido = require('../models/pedidos.model');
const Producto = require('../models/productos.model');

router.use(express.json());

/**
 * @swagger
 * /pedidos:
 *   post:
 *       summary: Crea un nuevo pedido a un usuario del sistema
 *       tags: [Pedidos]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/agregarPedido'
 *       responses:
 *           200:
 *               description: Pedido creado exitosamente
 *           400:
 *               description: Error en el ingreso de los datos
 *
 */

router.post('/', suspension, agregarPedidoVerVacio, verExistenciaProducto, verPagoUser, async (req, res) => {
  const {
    username,
    idDireccion,
    carro,
    nombreMedioPago,
  } = req.body;

  const user = await Usuario.findOne({ username });
  const productos = await Producto.find();
  const direccionUsuario = await user.direcciones.id(idDireccion);
  const direccion = direccionUsuario.direccion;
  const IdUser = user._id;
  const carrito = [];
  let products = [];
  let precio = 0;

  carro.forEach((product) => {
    productos.forEach((producto) => {
      if (product.nombre === producto.nombre) {
        products = {
          nombre: producto.nombre,
          cantidad: product.cantidad,
          precio: producto.precio,
        };
        carrito.push(products);
        precio += (product.cantidad * producto.precio);
      }
    });
  });

  const pedido = new Pedido({
    IdUser,
    direccion,
    precio,
    nombreMedioPago,
    carrito,
  });
  pedido.save();
  res.json(`Pedido creado exitosamente al usuario: ${user.username}, estado actual: ${pedido.estado}`);
});

/**
 * @swagger
 * /pedidos/usuarioPedidos:
 *   get:
 *       summary: Retorna el historial de pedidos del usuario enviado por parametros
 *       tags: [Pedidos]
 *       responses:
 *           200:
 *               description: Historial de pedidos del usuario
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/pedidosUser'
 *           400:
 *               description: Usuario o Contraseña incorrectos
 *
 */

router.get('/usuarioPedidos', async (req, res) => {
  const { username } = req.user;
  try {
    const user = await Usuario.findOne({ username });
    if (user.ifActive) {
      const pedido = await Pedido.find({ IdUser: user._id });
      if (pedido.length !== 0) res.json(pedido);
      else res.json('El usuario no tiene pedidos');
    } else res.status(400).json(`El Usuario:  ${username} se encuentra suspendido`);
  } catch (err) {
    res.status(400).json(`No se encontró el usuario ${username}`);
  }
});

/**
 * @swagger
 * /pedidos/borrarproducto:
 *   delete:
 *       summary: Borra un producto a un pedido de un usuario del sistema
 *       tags: [Pedidos]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/eliminarProducto'
 *       responses:
 *           200:
 *               description: Producto eliminado exitosamente
 *           400:
 *               description: Error al digitar los datos
 *
 */
router.delete('/borrarproducto', borrarProductoVerBody, async (req, res) => {
  const { idPedido, nombreProducto } = req.body;
  const pedido = await Pedido.findOne({ _id: idPedido });
  const producto = pedido.carrito.find((u) => u.nombre === nombreProducto);
  pedido.carrito.id(producto._id).remove();
  pedido.save();
  res.json(`Producto ${producto.nombre} eliminado satisfactoriamente`);
});

/**
 * @swagger
 * /pedidos/editarCantidad:
 *   put:
 *       summary: Edita la cantidad de un producto de un usuario del sistema
 *       tags: [Pedidos]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/editarCantidad'
 *       responses:
 *           200:
 *               description: Producto agregado exitosamente
 *           400:
 *               description: Error al digitar los datos
 *
 */
router.put('/editarCantidad', editarCantidadVerBody, async (req, res) => {
  const { idPedido, nombreProducto, cantidad } = req.body;
  const pedido = await Pedido.findOne({ _id: idPedido });
  const producto = pedido.carrito.find((u) => u.nombre === nombreProducto);
  const products = await pedido.carrito.id(producto._id);
  pedido.precio -= (products.cantidad * products.precio);
  products.cantidad = cantidad;
  pedido.precio += (products.cantidad * products.precio);
  pedido.save();
  res.json(`Producto ${nombreProducto} editado satisfactoriamente`);
});

/**
 * @swagger
 * /pedidos/agregarProducto:
 *   post:
 *       summary: Agrega un nuevo producto a un pedido de un usuario del sistema
 *       tags: [Pedidos]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/agregarProductoPedido'
 *       responses:
 *           200:
 *               description: Producto agregado exitosamente
 *           400:
 *               description: Error al digitar los datos
 *
 */
router.post('/agregarProducto', agregarProductoVerBody, productController, async (req, res) => {
  const { idPedido, nombreProducto, cantidad } = req.body;
  const pedido = await Pedido.findOne({ _id: idPedido });
  const productos = await Producto.findOne({ nombre: nombreProducto });
  const producto = pedido.carrito.find((u) => u.nombre === nombreProducto);
  if (producto) {
    const product = await pedido.carrito.id(producto._id);
    pedido.precio -= (product.cantidad * product.precio);
    product.cantidad += cantidad;
    pedido.precio += (product.cantidad * product.precio);
    pedido.save();
    res.json('Producto agregado satisfactoriamente');
  } else {
    pedido.carrito.push({ nombre: nombreProducto, cantidad, precio: productos.precio });
    pedido.precio += productos.precio;
    pedido.save();
    res.json('Producto agregado satisfactoriamente');
  }
});

/**
 * @swagger
 * /pedidos/cerrarPedido:
 *   put:
 *       summary: Cierra un pedido del sistema
 *       tags: [Pedidos]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/cerrarPedido'
 *       responses:
 *           200:
 *               description: Pedido cerrado exitosamente
 *           400:
 *               description: Error al digitar los datos
 *
 */
router.put('/cerrarPedido', cerrarPedidoVerBody, async (req, res) => {
  const { idPedido } = req.body;
  const pedido = await Pedido.findOne({ _id: idPedido });
  pedido.estado = 'Confirmado';
  pedido.save();
  res.json(`Pedido cerrado satisfactoriamente, estado actual: ${pedido.estado}`);
});

/**
 * @swagger
 * /pedidos/admin:
 *  get:
 *      summary: Obtener todos los pedidos del sistema
 *      tags: [Pedidos]
 *      responses:
 *          200:
 *              description: Lista de Pedidos del sistema organizados por usuario
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Pedido'
 */
router.get('/admin', autenticarAdministrador, obtenerPedidosVer, async (req, res) => {
  const pedido = await Pedido.find();
  const usuarios = await Usuario.find();
  const idusuarios = pedido.map((argument) => argument.IdUser);
  const nombres = [];
  const pedidos = [];

  idusuarios.forEach((id) => {
    usuarios.forEach((usuario) => {
      if (id == usuario._id) {
        nombres.push(usuario.username);
      }
    });
  });

  pedido.forEach((elemento, i) => {
    pedidos.push({
      username: nombres[i],
      _id: elemento._id,
      IdUser: elemento.IdUser,
      estado: elemento.estado,
      direccion: elemento.direccion,
      precio: elemento.precio,
      nombreMedioPago: elemento.nombreMedioPago,
      carrito: elemento.carrito,
    });
  });

  res.json(_.groupBy(pedidos, 'username'));
});

/**
 * @swagger
 * /pedidos/admin:
 *   put:
 *       summary: Edita el estado de un pedido del sistema
 *       tags: [Pedidos]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/editarEstado'
 *       responses:
 *           200:
 *               description: Pedido editado exitosamente
 *           400:
 *               description: Error al digitar los datos
 *
 */
router.put('/admin', autenticarAdministrador, editarEstadoVerBody, async (req, res) => {
  const { idPedido, estado } = req.body;
  const pedido = await Pedido.findOne({ _id: idPedido });
  pedido.estado = estado;
  pedido.save();
  res.json(`Pedido editado satisfactoriamente, estado actual: ${pedido.estado}`);
});

/**
 * @swagger
 * tags:
 *  name: Pedidos
 *  description: Sección de pedidos de los usuarios
 * components:
 *  schemas:
 *      Pedido:
 *          type: object
 *          properties:
 *              _id:
 *                  type: string
 *                  description: Id del pedido
 *              IdUser:
 *                  type: string
 *                  description: Id de general que identifica a todos los pedidos del usuario
 *              estado:
 *                  type: string
 *                  description: Estado del pedido
 *              direccion:
 *                  type: string
 *                  description: Domicilio al cual se enviará el pedido
 *              medioPago:
 *                  type: string
 *                  description: Forma en la que el usuario paga el pedido realizado
 *              precio:
 *                  type: integer
 *                  description: Precio total del pedido
 *              carrito:
 *                  type: array
 *                  description: Array de productos que se agregaran al pedido
 *          example:
 *              _id: f7f8jv9fdad730048hbg7o8
 *              IdUser: 4383jrfj38ff48hdfvnvvn
 *              estado: Entregado
 *              direccion: Torices, Carrera 14 #41-32
 *              medioPago: Cheque en Blanco
 *              precio: 12000
 *              carrito: [{ "nombre": "Hamburguesa Doble", "cantidad": 1}]
 *      pedidosUser:
 *          type: object
 *          properties:
 *              IdUser:
 *                  type: string
 *                  description: Id de general que identifica a todos los pedidos del usuario
 *              estado:
 *                  type: string
 *                  description: Estado del pedido
 *              direccion:
 *                  type: string
 *                  description: Domicilio al cual se enviará el pedido
 *              medioPago:
 *                  type: string
 *                  description: Forma en la que el usuario paga el pedido realizado
 *              precio:
 *                  type: integer
 *                  description: Precio total del pedido
 *              carrito:
 *                  type: array
 *                  description: Array de productos que se agregaran al pedido
 *          example:
 *              estado: Entregado
 *              IdUser: 4383jrfj38ff48hdfvnvvn
 *              _id: dfn3if39e3f9jefd93i4800ed
 *              direccion: Torices, Carrera 14 #41-32
 *              medioPago: Cheque en Blanco
 *              precio: 12000
 *              carrito: [{ "nombre": "Hamburguesa Doble", "cantidad": 1}]
 *      agregarPedido:
 *          type: object
 *          required:
 *              -username
 *              -carro
 *              -idDireccion
 *              -nombreMedioPago
 *          properties:
 *              username:
 *                  type: string
 *                  description: Nombre del usuario
 *              nombreMedioPago:
 *                  type: string
 *                  description: Medio de pago con el cual se cancelara el pedido
 *              idDireccion:
 *                  type: string
 *                  description: Id de la direccion a la cual se en enviará el pedido
 *              carro:
 *                  type: array
 *                  description: Array de productos que se agregaran al pedido
 *          example:
 *              username: Decstro
 *              nombreMedioPago: Cheque en Blanco
 *              idDireccion: 611ded8d1761d415b0e82d82
 *              carro: [{ "nombre": "Hamburguesa Doble", "cantidad": 1}]
 *      eliminarProducto:
 *          type: object
 *          required:
 *              -idPedido
 *              -nombreProducto
 *          properties:
 *              idPedido:
 *                  type: string
 *                  description: Id del pedido actual del usuario
 *              nombreProducto:
 *                  type: string
 *                  description: Nombre del producto a eliminar
 *          example:
 *              idPedido: 61254f5d4245d63bd4a15837
 *              nombreProducto: Lomo de Cerdo
 *      editarCantidad:
 *          type: object
 *          required:
 *              -idPedido
 *              -nombreProducto
 *              -cantidad
 *          properties:
 *              idPedido:
 *                  type: string
 *                  description: Id del pedido actual del usuario
 *              nombreProducto:
 *                  type: string
 *                  description: Nombre del producto a eliminar
 *              cantidad:
 *                  type: integer
 *                  description: La cantidad del producto
 *          example:
 *              idPedido: 61254f5d4245d63bd4a15837
 *              nombreProducto: Lomo de Cerdo
 *              cantidad: 5
 *      cerrarPedido:
 *          type: object
 *          required:
 *              -idPedido
 *          properties:
 *              idPedido:
 *                  type: string
 *                  description: Id del pedido actual del usuario
 *          example:
 *              idPedido: 3439rkokf8efhd7gwff2h
 *      agregarProductoPedido:
 *          type: object
 *          required:
 *              -idPedido
 *              -nombreProducto
 *              -cantidad
 *          properties:
 *              idPedido:
 *                  type: string
 *                  description: Id del pedido
 *              nombreProducto:
 *                  type: string
 *                  description: Nombre del producto a agregar
 *              cantidad:
 *                  type: number
 *                  description: Cantidad del producto a agregar
 *          example:
 *              idPedido: jijf8gfj8bf8fbufbasx3
 *              nombreProducto: Hamburguesa Doble
 *              cantidad: 2
 *      editarEstado:
 *          type: object
 *          required:
 *              -idPedido
 *              -estado
 *          properties:
 *              idPedido:
 *                  type: string
 *                  description: Id del pedido actual del usuario
 *              estado:
 *                  type: string
 *                  description: Nuevo estado del pedido
 *          example:
 *              idPedido: jijf8gfj8bf8fbufb
 *              estado: Enviado
 */

module.exports = router;
