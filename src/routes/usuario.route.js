/* eslint-disable no-underscore-dangle */
/* eslint linebreak-style: ["error", "windows"] */
const express = require('express');

const router = express();
const bcrypt = require('bcrypt');
require('../db');
const _ = require('lodash');

router.use(express.json());
const { verUserVacio, suspension } = require('../controllers/users.controllers');
const autenticarAdministrador = require('../middlewares/probarAdmin.middlewares');
const Usuario = require('../models/usuarios.model');

/**
 * @swagger
 * /usuarios:
 *   get:
 *       summary: Retorna todos los usuarios del sistema
 *       tags: [Usuarios]
 *       responses:
 *           200:
 *               description: Muestra la lista de los usuarios del sistema
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/usuario'
 */

router.get('/', async (req, res) => {
  const usuario = await Usuario.find();
  res.json(_.groupBy(usuario, 'ifActive'));
});

/**
 * @swagger
 * /usuarios:
 *   post:
 *       summary: Agrega un nuevo usuario al sistema
 *       tags: [Usuarios]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/usuario'
 *       responses:
 *           200:
 *               description: Usuario creado exitosamente
 *           401:
 *               description: El valor o propiedad  ya se encuentra en la base de datos
 */

router.post('/', autenticarAdministrador, verUserVacio, async (req, res) => {
  const {
    username,
    password,
    email,
    age,
    telefono,
    direcciones,
  } = req.body;

  const user = new Usuario({
    username,
    password: bcrypt.hashSync(password, 5),
    email,
    age,
    telefono,
    direcciones,
  });
  user.save();
  res.json('Nuevo usuario agregado exitosamente');
});

/**
 * @swagger
 * /usuarios/suspender/{username}:
 *   put:
 *       summary: El administrador suspende un usuario
 *       tags: [Usuarios]
 *       parameters:
 *         - name: username
 *           in: path
 *           required: true
 *           description: Nombre del usuario
 *           schema:
 *               type: string
 *       responses:
 *           200:
 *               description: Usuario suspendido satisfactoriamente
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/usuario'
 *           400:
 *               description: No existe el usuario o se digito incorrectamente
 *
 */

router.put('/suspender/:username', autenticarAdministrador, async (req, res) => {
  const { username } = req.params;
  try {
    const usuario = await Usuario.findOne({ username });
    if (usuario.ifActive) {
      usuario.ifActive = false;
      usuario.save();
      res.json(`Usuario ${username} suspendido`);
    } else res.json(`El usuario ${username} ya se encuentra suspendido`);
  } catch (err) {
    res.status(404).json(`El usuario ${username} no se encontró`);
  }
});

/**
 * @swagger
 * /usuarios/activar/{username}:
 *   put:
 *       summary: El administrador activa un usuario
 *       tags: [Usuarios]
 *       parameters:
 *         - name: username
 *           in: path
 *           required: true
 *           description: Nombre del usuario
 *           schema:
 *               type: string
 *       responses:
 *           200:
 *               description: Usuario activado satisfactoriamente
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/usuario'
 *           400:
 *               description: No existe el usuario o se digito incorrectamente
 *
 */
router.put('/activar/:username', autenticarAdministrador, async (req, res) => {
  const { username } = req.params;
  try {
    const usuario = await Usuario.findOne({ username });
    if (!usuario.ifActive) {
      usuario.ifActive = true;
      usuario.save();
      res.json(`Usuario ${username} activado`);
    } else res.json(`El usuario ${username} ya se encuentra activo`);
  } catch (err) {
    res.status(404).json(`El usuario ${username} no se encontró`);
  }
});

/**
 * @swagger
 * /usuarios/agregarDireccion:
 *   put:
 *       summary: Agregar una dirección a un usuario del sistema
 *       tags: [Usuarios]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/agregarDireccion'
 *       responses:
 *           200:
 *               description: Direccion agregada exitosamente
 *           400:
 *               description: Error en el ingreso de los datos
 *
 */
router.put('/agregarDireccion', suspension, async (req, res) => {
  const { username, direccion } = req.body;
  if (direccion !== '' && direccion !== null) {
    try {
      const usuario = await Usuario.findOne({ username });
      usuario.direcciones.push({ direccion });
      usuario.save();
      res.json(`La direccion: ${direccion} fue agregada al usuario ${username} `);
    } catch (err) {
      res.status(404).json(`El usuario ${username} no se encontró`);
    }
  } else res.status(400).json('El campo direccion se encuentra vacio');
});

/**
 * @swagger
 * /usuarios/eliminarDireccion:
 *   put:
 *       summary: Eliminar una dirección a un usuario del sistema
 *       tags: [Usuarios]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/eliminarDireccion'
 *       responses:
 *           200:
 *               description: Direccion eliminada exitosamente
 *           400:
 *               description: Error en el ingreso de los datos
 *
 */
router.put('/eliminarDireccion', suspension, async (req, res) => {
  const { username, idDireccion } = req.body;
  if (idDireccion !== '' && idDireccion !== null) {
    try {
      const usuario = await Usuario.findOne({ username });
      // eslint-disable-next-line eqeqeq
      const direccion = usuario.direcciones.find((u) => u._id == idDireccion);
      if (direccion) {
        usuario.direcciones.id(idDireccion).remove();
        usuario.save();
        res.json(`La direccion ${direccion.direccion} fue eliminada del usuario ${username} `);
      } else res.json(`El usuario no posee una direccion con este id: ${idDireccion}`);
    } catch (err) {
      res.status(404).json(`El usuario ${username} no se encontró`);
    }
  } else res.status(400).json('El campo idDireccion se encuentra vacio');
});

/**
 * @swagger
 * tags:
 *  name: Usuarios
 *  description: Sección de Usuarios
 * components:
 *  schemas:
 *      usuario:
 *          type: object
 *          required:
 *              -username
 *              -password
 *              -email
 *              -age
 *              -telefono
 *              -direcciones
 *          properties:
 *              _id:
 *                  type: string
 *                  description: Id del usuario
 *              username:
 *                  type: string
 *                  description: Nombre del usuario
 *              password:
 *                  type: string
 *                  description: Contraseña del usuario
 *              email:
 *                  type: string
 *                  description: Email del usuario
 *              age:
 *                  type: integer
 *                  description: Edad del usuario
 *              telefono:
 *                  type: integer
 *                  description: Telefono del usuario
 *              ifActive:
 *                  type: boolean
 *                  description: Indica si el usuario esta activo o no
 *              ifadmin:
 *                  type: boolean
 *                  description: Indica si el usuario es administrador o no
 *              direcciones:
 *                  type: array
 *                  description: Agenda de direcciones del usuario
 *          example:
 *              username: Ragnar
 *              password: KingofNorway
 *              email: sonofOdin@gmail.com
 *              age: 46
 *              telefono: 3316738933
 *              direcciones: [{ "direccion": "Kattegat"}]
 *      agregarDireccion:
 *          type: object
 *          required:
 *              -username
 *              -direccion
 *          properties:
 *              username:
 *                  type: string
 *                  description: Usuario que va a agregar una direccion
 *              direccion:
 *                  type: string
 *                  description: Direccion a agregar
 *          example:
 *              username:  Monseiur
 *              direccion: Parc des Princes
 *      eliminarDireccion:
 *          type: object
 *          required:
 *              -username
 *              -direccion
 *          properties:
 *              username:
 *                  type: string
 *                  description: Usuario que va a agregar una direccion
 *              direccion:
 *                  type: string
 *                  description: Id de la dirección a eliminar
 *          example:
 *              username:  Monseiur
 *              idDireccion: deo9023i7fh74hff038r998
 *
 */

module.exports = router;
