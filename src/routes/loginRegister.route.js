/* eslint-disable prefer-destructuring */
/* eslint linebreak-style: ["error", "windows"] */
const express = require('express');
const bcrypt = require('bcrypt');
const jsonwebtoken = require('jsonwebtoken');
const config = require('../config');
require('../db');
const { verUserVacio, verSuspension } = require('../controllers/users.controllers');

const router = express();

const Usuario = require('../models/usuarios.model');

router.use(express.json());

/**
 * @swagger
 * /login-register/{username}/{password}:
 *   get:
 *       summary: Logea a los usuarios que tienen una cuenta creada en el sistema
 *       security: [] # No security
 *       tags: [Login-Register]
 *       parameters:
 *         - name: username
 *           in: path
 *           required: true
 *           description: Nombre del usuario
 *           schema:
 *               type: string
 *         - name: password
 *           in: path
 *           required: true
 *           description: Contraseña del usuario
 *           schema:
 *               type: string
 *       responses:
 *           200:
 *               description: Usuario logeado correctamente
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/login'
 *           400:
 *               description: Usuario o Contraseña incorrectos
 *
 */

router.get('/:username/:password', verSuspension, async (req, res) => {
  const { username, ifadmin } = req.params;
  const token = jsonwebtoken.sign({
    username,
    ifadmin,
  }, config.module.mySecretPassword);
  res.json(`Usuario logeado, token:   ${token}`);
});

/**
 * @swagger
 * /login-register:
 *   post:
 *       summary: Registra un nuevo usuario en el sistema
 *       security: [] # No security
 *       tags: [Login-Register]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/register'
 *       responses:
 *           200:
 *               description: Usuario registrado exitosamente
 *           400:
 *               description: Usuario o contraseña incorrectos
 */
router.post('/', verUserVacio, async (req, res) => {
  const {
    username,
    password,
    email,
    age,
    telefono,
    direcciones,
  } = req.body;
  try {
    const user = new Usuario({
      username,
      password: bcrypt.hashSync(password, 5),
      email,
      age,
      telefono,
      direcciones,
    });
    user.save();
    res.json('Nuevo Usuario registrado exitosamente');
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    res.status(400).json('Error al ingresar un campo');
  }
});

/**
 * @swagger
 * tags:
 *  name: Login-Register
 *  description: Sección de registro e ingreso de Usuarios
 * components:
 *  schemas:
 *      login:
 *          type: object
 *          required:
 *              -username
 *              -password
 *          properties:
 *              username:
 *                  type: string
 *                  description: Nombre del usuario
 *              password:
 *                  type: string
 *                  description: Contraseña del usuario
 *          example:
 *              username: Decstro
 *              password: 12345
 *      register:
 *          type: object
 *          required:
 *              -username
 *              -password
 *              -email
 *              -age
 *              -telefono
 *              -direcciones
 *          properties:
 *              username:
 *                  type: string
 *                  description: Nombre del usuario
 *              password:
 *                  type: string
 *                  description: Contraseña del usuario
 *              email:
 *                  type: string
 *                  description: Email del usuario
 *              age:
 *                  type: integer
 *                  description: Edad del usuario
 *              telefono:
 *                  type: integer
 *                  description: Telefono del usuario
 *              direcciones:
 *                  type: array
 *                  description: Agenda de direcciones del usuario
 *              ifActive:
 *                  type: boolean
 *                  description: Indica si el usuario se encuentra suspendido o activo
 *              ifAdmin:
 *                  type: boolean
 *                  description: Esta propiedad indica si el usuario es administrador o no
 *          example:
 *              username: Ivar
 *              password: TheBoneless
 *              email: ragnarson@hotmail.com
 *              age: 25
 *              telefono: 3457895643
 *              direcciones: [{ "direccion": "Norway, Kattegat the throne"}]
 *
 */

module.exports = router;
