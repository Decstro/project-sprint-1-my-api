/* eslint-disable no-console */
/* eslint linebreak-style: ["error", "windows"] */
const express = require('express');

const router = express();
const _ = require('lodash');
const clienteRedis = require('../redis');
const autenticarAdministrador = require('../middlewares/probarAdmin.middlewares');
const { agregarProductoVerVacio, editarProductoVerBody } = require('../controllers/productos.controllers');
const cache = require('../middlewares/cacheProductos.middlewares');
const Producto = require('../models/productos.model');

/**
 * @swagger
 * /productos:
 *  get:
 *      summary: Obtener todos los productos del sistema
 *      tags: [Productos]
 *      responses:
 *          200:
 *              description: Lista de productos del sistema organizados por tipo
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Producto'
 */
router.get('/', cache, async (req, res) => {
  const producto = await Producto.find();
  clienteRedis.setex('Productos', 60 * 5, JSON.stringify(producto));
  res.json(_.groupBy(producto, 'tipo'));
});

/**
 * @swagger
 * /productos/admin:
 *   post:
 *       summary: Crea un nuevo producto en el sistema
 *       tags: [Productos]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/agregarProducto'
 *       responses:
 *           200:
 *               description: Producto creado exitosamente
 *           400:
 *               description: Error al digitar los datos
 *
 */
router.post('/admin', autenticarAdministrador, agregarProductoVerVacio, async (req, res) => {
  const { nombre, precio, tipo } = req.body;
  const agregarProducto = new Producto({ nombre, precio, tipo });
  agregarProducto.save();
  res.json('Producto creado exitosamente');
});

/**
 * @swagger
 * /productos/admin:
 *   put:
 *       summary: Edita el nombre y el precio de un producto
 *       tags: [Productos]
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/editarProducto'
 *       responses:
 *           200:
 *               description: Producto agregado exitosamente
 *           400:
 *               description: Error al digitar los datos
 *
 */
router.put('/admin', autenticarAdministrador, editarProductoVerBody, async (req, res) => {
  const { nombre, id, precio } = req.body;
  const actualizarProducto = await Producto.findById(id);
  actualizarProducto.nombre = nombre;
  actualizarProducto.precio = precio;
  actualizarProducto.save();
  clienteRedis.del('Productos');
  res.json('Producto editado exitosamente');
});

/**
 * @swagger
 * /productos/admin/{idProduct}:
 *   delete:
 *       summary: Elimina el producto segun el id enviado por parametros
 *       tags: [Productos]
 *       parameters:
 *         - name: idProduct
 *           in: path
 *           required: true
 *           description: Id del producto a eliminar
 *           schema:
 *               type: string
 *       responses:
 *           200:
 *               description: Elimina un producto del sistema
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/Producto'
 *           400:
 *               description: Usuario o Contraseña incorrectos
 *
 */
router.delete('/admin/:idProduct', autenticarAdministrador, async (req, res) => {
  const id = req.params.idProduct;
  try {
    const producto = await Producto.findById(id);
    if (producto) {
      const eliminarProducto = await Producto.findByIdAndDelete(id);
      console.log(eliminarProducto);
      res.json('Producto eliminado exitosamente');
    } else res.status(400).json(`No existe un producto con este id: ${id}`);
  } catch (err) {
    res.status(400).json(`No existe un producto con este id: ${id}`);
  }
});

/**
 * @swagger
 * tags:
 *  name: Productos
 *  description: Sección de productos del sistema
 * components:
 *  schemas:
 *      Producto:
 *          type: object
 *          required:
 *              -nombre
 *              -id
 *              -precio
 *              -tipo
 *          properties:
 *              nombre:
 *                  type: string
 *                  description: Nombre del producto
 *              id:
 *                  type: integer
 *                  description: Id del producto
 *              precio:
 *                  type: integer
 *                  description: Precio del producto
 *              tipo:
 *                  type: string
 *                  description: Categoria del producto
 *          example:
 *              nombre: Carne
 *              id: 4
 *              precio: 4000
 *              tipo: Asados
 *      agregarProducto:
 *          type: object
 *          required:
 *              -nombre
 *              -precio
 *              -tipo
 *          properties:
 *              nombre:
 *                  type: string
 *                  description: Nombre del producto
 *              precio:
 *                  type: integer
 *                  description: Precio del producto
 *              tipo:
 *                  type: string
 *                  description: Categoria del producto
 *          example:
 *              nombre: Carne
 *              precio: 5000
 *              tipo: Asados
 *      editarProducto:
 *          type: object
 *          required:
 *              -nombre
 *              -id
 *              -precio
 *          properties:
 *              nombre:
 *                  type: string
 *                  description: Nombre del producto
 *              id:
 *                  type: string
 *                  description: Id del producto
 *              precio:
 *                  type: integer
 *                  description: Precio del producto
 *          example:
 *              nombre: Carne
 *              id: 6116d4ed3693f53da87f3071
 *              precio: 5000
 */

module.exports = router;
