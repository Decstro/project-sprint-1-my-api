# <font color="Darkblue"> API Delilah Live :cake:</font>

<font color="Darkblue">*Sprint Project 3 - Acámica*</font>
 
## <font color="Darkblue">Descripción :page_facing_up:</font>
Esta API  realiza todas las operaciones necesarias para la administración de los pedidos en el restaurante <font color="Blue">**Delilah Restó**</font>. Esta api es la  conexión entre el usuario final y la mayoria de los servicios que se ofrecen en el establecimiento, mediante la estandarización de los procesos se brinda una excelente calidad a los usuarios asi como una fantástica experiencia.

## <font color="Darkblue">AWS Console </font>
Este proyecto se encuentra realizado principalmente en Node js y en AWS, a traves de swagger se proporciona una interfaz que permite controlara todas las funcionalidades de la api.

Con el objetivo de que se pueda observar y evaluar este proyecto, a continuación se presentan los datos de ingreso a la consola de aws, este usuario tiene politicas que permiten analizar la arquitectura pero no hacer cambios en la misma, las credenciales son las siguientes: 

 - **Id de usuario:** 495875654377
 - **Usuario:** TechReviewer
 - **Contraseña:** @TechReviewer67

Para conectarse a traves de ssh se debe usar el comando:
 
 > ssh -i .\myApiDelilah.pem ubuntu@18.231.179.143

Y ademas se necesita el archivo .pem

## <font color="Darkblue"> Arquitectura </font>
El siguiente grafico muestra la arquitectura empleada para que la api funcionara en la nube, basicamente lo que se hace es conectar nuestra api a una maquina virtual de amazon, luego esta maquina virtual es utilizada como base para crear una imagen virtual, la cual es conectada a los balanceadores de carga que posteriormente redirigen las peticiones a el servicio de route53 el cual a través de los dominios permite que la api se encuentre en la web.
![ALT Arquitectura](https://bucket.decstro-delilah.ml/ArchitectureAWS.jpeg)

## <font color="Darkblue"> Tecnologías :computer: </font>
Esta api se hizo usando principalmente el lenguaje <font color="Blue">**Javascript**</font> y empleando <font color="Blue">JSON</font> para recibir y enviar información, dentro de las tecnologias o herramientas que hicieron esto posible se encuentra <font color="Blue">NodeJs, ExpressJs, Npm, MongoDb</font> (y su ODM mongoose),   <font color="Blue">JWT</font>,  frameworks para la organización como <font color="Blue">lodash y swagger</font>, middlewares como <font color="Blue">Helmet</font> asi como otras librerias para la encriptación y el testing de la API. 


## <font color="Darkblue"> Instalación :hammer: </font>
Variables de entorno:

- PORT: Esta variable indica el puerto de nuestro servidor.

- DBConnection: Esta es la conexión a la base de datos, por defecto mongodb se conecta al puerto 27017.

- mySecretPassword: En esta variable se almacena la contraseña con la cual se encriptaran los endpoints a través de JWT.

- redisPORT: Esta variable almacena el puerto en el que redis (la libreria para el testing de la api) correrá el testing.

- AdminUser: Nombre del usuario administrador

- AdminPassword: Es la clave del usuario administrador


### <font color="Darkblue"> Funcionamiento de la Api :cyclone: </font>

La Api tiene 4 modelos, los cuales son:

- <font color="Darkblue"> Modelo Usuarios </font>
- <font color="Darkblue"> Modelo Pedidos </font>
- <font color="Darkblue"> Modelo Productos </font>
- <font color="Darkblue"> Modelo Medios de Pago </font>

Cuando iniciamos la Api por primera vez en nuestro entorno, se crea un usuario administrador, se crean los productos del sistema  y los medios de pago, todo esto se crea por defecto. 

En la Api existen dos tipos de usuarios, los <font color="blue">administradores</font> y los <font color="blue">no administradores</font>, su diferencia principalmente recae en los permisos que estos poseen.

El usuario administrador que se crea por defecto al iniciar la Api es el siguiente:

<font color="Darkblue">Username:</font> Decstro
<font color="Darkblue">Password:</font> 12345

Para mas información con respecto a los modelos, esquemas y endpoints, se recomienda revisar la documentación de swagger de esta Api: [Swagger Documentation Api-Delilah-Restó](https://www.decstro-delilah.ml/api-docs/).


Si tienes alguna inquietud o duda adicional, puedes contactarme: juan.puello99@hotmail.com 
